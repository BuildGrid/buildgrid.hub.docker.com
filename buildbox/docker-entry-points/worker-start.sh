#!/bin/bash

set +x

BUILDBOX_TEMPORARY_DIRECTORY=$(sudo -u casuser mktemp -d)
BUILDBOX_CASD_LOCAL_CACHE="${BUILDBOX_TEMPORARY_DIRECTORY}/casd"

# Sleep to give buildgrid time to startup
sleep 1

# Launch Casd as casuser
sudo --preserve-env=BUILDBOX_STAGER -u casuser "$CASD_BINARY" \
--verbose \
--cas-remote="$CAS_SERVER_URL" \
--instance="$INSTANCE_NAME" \
--cas-instance="$INSTANCE_NAME" \
--bind="$BIND_LOCATION" "$BUILDBOX_CASD_LOCAL_CACHE" &

# Normally we want to run casd and the worker with a single user,
# but that is dangerous when using the hardlink stager.
run_user=casuser
if [[ "$BUILDBOX_STAGER" = "hardlink" ]]
then
    run_user=runuser
fi


LOGSTREAM_SERVER_OPTIONAL_LOCATION=""
if [[ -n "$LOGSTREAM_SERVER_URL" ]]
then
    LOGSTREAM_SERVER_OPTIONAL_LOCATION="--logstream-remote=${LOGSTREAM_SERVER_URL} --logstream-instance=${LOGSTREAM_SERVER_INSTANCE_NAME}"
fi


# Launch Worker/Runner as either runuser or casuser
sudo -u "$run_user" "$WORKER_BINARY" \
--verbose \
--buildbox-run="$RUNNER_BINARY" \
--runner-arg=--workspace-path=/casuser/data/chroot/image \
--bots-remote="$BUILDGRID_SERVER_URL" \
--cas-remote="http://${BIND_LOCATION}" \
--cas-instance="${CAS_SERVER_INSTANCE_NAME}" \
--request-timeout=30 \
--instance="${INSTANCE_NAME}" \
$LOGSTREAM_SERVER_OPTIONAL_LOCATION \
$PLATFORM_PROPERTY_OPTS
