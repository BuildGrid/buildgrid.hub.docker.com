##
# Build manifest for buildbox:nightly.
#
# Basic usage:
#  - docker build -f Dockerfile.nightly -t buildbox:nightly .
#  - docker image rm buildbox:nightly
#

FROM debian:bookworm

WORKDIR /tmp

RUN \
	apt-get update \
	&& \
	apt-get install -y \
		build-essential git cmake pkg-config \
		libssl-dev libfuse3-dev uuid-dev \
		libprotobuf-dev protobuf-compiler \
		libgrpc-dev libgrpc++-dev protobuf-compiler-grpc \
		libgtest-dev libgmock-dev libtomlplusplus-dev \
		python3 python3-pip \
		bubblewrap \
		sudo \
		fuse3 \
	&& \
	apt-get clean

RUN \
	git clone --depth=1 \
		https://gitlab.com/BuildGrid/buildbox/buildbox.git \
		/tmp/buildbox \
	&& \
	cmake -B /tmp/buildbox/build /tmp/buildbox \
		-DBUILD_TESTING=OFF \
	&& \
	make -j$(nproc) -C /tmp/buildbox/build install \
	&& \
	rm -rf /tmp/buildbox \
	&& \
	sh -c "buildbox-casd --help &> /dev/null"

RUN \
	git clone --depth=1 \
		https://gitlab.com/BuildGrid/buildbox/userchroot.git \
		/tmp/userchroot \
	&& \
	cmake -B /tmp/userchroot/build /tmp/userchroot \
		-DBUILD_TESTING=OFF \
	&& \
	make -j$(nproc) -C /tmp/userchroot/build install \
	&& \
	rm -rf /tmp/userchroot

# create wheel group for casuser
RUN groupadd wheel && \
	echo "%wheel    ALL=(ALL) ALL" | sudo EDITOR='tee -a' visudo

# add runuser,  add casuser then add it to wheel group
RUN useradd --no-log-init runuser && \
	useradd --no-log-init casuser && \
	usermod -aG wheel casuser

# make chroot directory
RUN mkdir -p -m 0755 /casuser/data/chroot && chown -R casuser:wheel /casuser/data/chroot && \
	mkdir -p -m 0755 /casuser/data/chroot/image && chown -R casuser:wheel /casuser/data/chroot/image

# /etc/userchroot.conf (should pick this one, will fail otherwise)
RUN mkdir -p /etc/ && \
echo "casuser:/casuser/data/chroot" >> /etc/userchroot.conf && chmod 0644 /etc/userchroot.conf

# set permissions on userchroot executable
RUN chmod 4775 $(which userchroot)

ENV CASD_BINARY=buildbox-casd
ENV RUNNER_BINARY=buildbox-run-hosttools
ENV WORKER_BINARY=buildbox-worker

ENV INSTANCE_NAME=""

# Set urls
ENV BUILDGRID_SERVER_URL="http://controller:50051"
ENV CAS_SERVER_URL=${BUILDGRID_SERVER_URL}
ENV LOGSTREAM_SERVER_URL=""
ENV LOGSTREAM_SERVER_INSTANCE_NAME=""
ENV BIND_LOCATION=127.0.0.1:50011

# Set stager
ENV BUILDBOX_STAGER=fuse

# Set platform properties
ARG DEFAULT_PLATFORM_OPTS="--platform OSFamily=linux --platform ISA=x86-64"
ENV PLATFORM_PROPERTY_OPTS=${DEFAULT_PLATFORM_OPTS}

# Default entry point
COPY ./docker-entry-points /entry-points
CMD /entry-points/worker-start.sh
